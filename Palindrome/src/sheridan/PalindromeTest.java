package sheridan;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class PalindromeTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testIsPalindrome( ) {
		assertTrue("Invalid value for palindrome" , Palindrome.isPalindrome("Anna") );
	}

	@Test
	public void testIsPalindromeNegative( ) {
		assertFalse("Invalid value for palindrome" , Palindrome.isPalindrome("not a palindrome") );
	}
	@Test
	public void testIsPalindromeBoundaryIn( ) {
		assertTrue("Invalid value for palindrome" , Palindrome.isPalindrome("A") );
	}
	@Test
	public void testIsPalindromeBoundaryOut( ) {
		assertFalse("Invalid value for palindrome" , Palindrome.isPalindrome("Anne") );
	}	
	
}
